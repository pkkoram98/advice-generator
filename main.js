const id = document.getElementById('advice_id')
const advice = document.getElementById('advice_text')
const divider = document.getElementById('divider')
const getAdvice = async () => {
    const res = await fetch('https://api.adviceslip.com/advice')
    const { slip } = await res.json()
    id.innerText = slip.id
    advice.innerText = '"' + slip.advice + '"'
}

getAdvice()

function changeDividerImg() {
    var w = document.documentElement.clientWidth;

    if (w <= 375) {
        divider.src = 'images/pattern-divider-mobile.svg'
    } else {
        divider.src = 'images/pattern-divider-desktop.svg'
    }
}

window.addEventListener("resize", changeDividerImg);

changeDividerImg();